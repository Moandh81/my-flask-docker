"""  init file for the application """

import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def create_app():
    """ factor app function """
    app = Flask (__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://postgres:postgres@db/postgres"
    app.config["SECRET_KEY"] = os.urandom (32)
    from myflaskdocker.main.index import main_bp #pylint: disable=C0415
    from myflaskdocker.posts.views import posts_bp #pylint: disable=C0415
    db.init_app(app)
    app.register_blueprint (main_bp)
    app.register_blueprint (posts_bp)
    return app
