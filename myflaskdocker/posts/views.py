from flask import Blueprint, render_template, request, abort
from .forms import PostForm
from datetime import datetime
from slugify import slugify
from myflaskdocker.models import Post
from myflaskdocker import db

posts_bp = Blueprint('posts', __name__)


@posts_bp.route('/post', methods=['POST', 'GET'])
def post():
    form = PostForm()
    if request.method == 'POST' and form.validate_on_submit():
        post = Post()
        post.title = request.form["title"]
        post.body = request.form["body"]
        post.slug = slugify(request.form['title'].lower())
        post.created_at = datetime.utcnow()
        db.session.add(post)
        db.session.commit()
        return "form submitted successfully"
    return render_template('post.html', form=form)


@posts_bp.route('/post/<string:slug>', methods=["GET"])
def getPost(slug):
    post = Post.query.filter_by(slug=slug).first()
    if post is None:
        abort(404, description="Post not found")

    return render_template("single_post.html", post=post)


