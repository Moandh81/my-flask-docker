from flask_wtf import FlaskForm
from wtforms.fields import StringField, TextAreaField, SubmitField
from wtforms.validators import  InputRequired

class PostForm(FlaskForm):
    title = StringField('title', validators=[InputRequired()])
    body = TextAreaField("body", validators=[InputRequired()] )
    submit = SubmitField('Envoyer')