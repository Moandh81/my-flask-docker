from flask import Blueprint, render_template


main_bp = Blueprint('main', __name__)

@main_bp.route('/')
def hello():
    """ returns hello message from the template"""

    return render_template("index.html")
