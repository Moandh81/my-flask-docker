FROM python:3.7


COPY . /app

WORKDIR /app

ENV FLASK_APP=run.py
ENV FLASK_ENV=development
ENV FLASK_DEBUG=1

RUN /usr/local/bin/python -m pip install --upgrade pip

RUN pip3 install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["flask" , "run", "--host=0.0.0.0"]