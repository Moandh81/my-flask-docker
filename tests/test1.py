import unittest

from flask import Flask

from myflaskdocker import create_app


class TestFlaskMethod(unittest.TestCase):
    def setUp(self):
        app =create_app()
        app.config['TESTING']= True
        app.config['WTF_CSRF_METHODS']= []
        self.client= app.test_client()


    def test_the_home_page(self):
        """ check if home page responds correctly """
        result = self.client.get('/')
        assert result.status_code == 200
        print(self._testMethodName)



    def test_the_post_page(self):
        result = self.client.get ( '/post' )
        assert result.status_code == 200
        print(self._testMethodName)


    def test_not_found_page(self):
        result = self.client.get ( '/notfoundaddress' )
        assert result.status_code == 404
        print(self._testMethodName)



if __name__ == '__main__':
    unittest.main()