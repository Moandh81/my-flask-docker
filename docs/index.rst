.. Flask Blog documentation master file, created by
   sphinx-quickstart on Sun Apr 18 10:10:37 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Flask Blog's documentation!
======================================

This is an example test

This is a subtitle
-----------------------------

This is a paragraph after the subtitle
This is an example of a link `Google <https://google.com>`_

I will include an example like this::

   def hello():
     return "hello world"


This is another example code::

   def goodbye():
     return "Goodbye"



Another Title
=============

In this section we will discuss the following points::

- Point 1
- Point 2
- Point 3



.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`




